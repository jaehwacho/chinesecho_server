#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask.ext.script import Manager
from flask.ext.migrate import MigrateCommand
from app import app, DBManager

import sys
reload(sys)
sys.setdefaultencoding("utf-8")

#실행방법
#python manage.py db init
#python manage.py db migrate
#python manage.py db upgrade
manager = Manager(app)
manager.add_command('db', MigrateCommand)

#실행방법
#python manage.py init_db
@manager.command
def init_db():
    """Initialize database."""
    with app.app_context():
        DBManager.init_db()

#실행방법
#python manage.py clear_db
@manager.command
def clear_db():
    """Clear database."""
    with app.app_context():
        DBManager.clear_db()

if __name__ == '__main__':
    manager.run()