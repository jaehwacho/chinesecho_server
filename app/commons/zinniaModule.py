#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask.json import jsonify


def checkWord(sexpData):
        import zinnia
        returnData=[]
        zin_reco = zinnia.Recognizer
        zin_chr = zinnia.Character
        zin_result = zinnia.Result

        recognizer = zin_reco();
        zin_reco.open(recognizer, "./app/commons/resource/handwriting-zh_CN.model")
        sexpData = str(sexpData)
        zin_chr = zinnia.Character();
        zin_chr.parse(sexpData);
        result = zin_reco.classify(recognizer, zin_chr, 10);
        if result == None :
            return "None"
        for i in range(zin_result.size(result)) :
            returnData.append(zin_result.value(result, i))

        return returnData
