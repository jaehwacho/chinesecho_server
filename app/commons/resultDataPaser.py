#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import jsonify
from app import ServerConfig

def resultLearningImageCard(result):
    result_list = []
    for item in result:
        item = {"idWord": item[0], "chinese": item[1],"pronunciation":item[2],
                "pro_file_path":item[3],"meaning":item[4],"difficulty":item[5],
                "imgSrc":ServerConfig.SERVER_IP+ServerConfig.IMG_SRC+str(item[6]),"description":item[7],
                "createByUserId":item[8],"designByUserId":item[9],"imgCount":item[10]}
        result_list.append(item)
    return jsonify(result=result_list)

def resultImageCard(result):
    result_list = []
    for item in result:
        item = {"idWord": item[0], "chinese": item[1],"pronunciation":item[2],
                "pro_file_path":item[3],"meaning":item[4],"difficulty":item[5],
                "imgSrc":ServerConfig.SERVER_IP+ServerConfig.IMG_SRC+str(item[6]),"description":item[7],
                "createByUserId":item[8],"designByUserId":item[9]}
        result_list.append(item)
    return jsonify(result=result_list)

def resultNoneImageCard(result):
    result_list = []
    for item in result:
        item = {"idWord": item[0], "chinese": item[1],"pronunciation":item[2],
                "pro_file_path":item[3],"meaning":item[4],"difficulty":item[5]}
        result_list.append(item)
    return jsonify(result=result_list)

