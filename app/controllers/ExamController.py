#!/usr/bin/python
# -*- coding: utf-8 -*
# -
import traceback

from flask import jsonify

from app.database import DBManager
from app.serverConfig import ServerConfig

def selectExamWord(userId,amount):

    #from app.commons.resultDataPaser import resultLearningImageCard
    #난이도 2이하의 모든 카드들중 10개

    #select idWord, chinese, pronunciation, meaning, difficulty from word order by rand() limit 10;

    #select idWord, pronunciation, meaning from word order by rand() limit 40;

    #랜덤으로 amoint만큼 단어 추출
    query = "select idWord, chinese, pronunciation, meaning, difficulty from word order by rand() limit %d;"%(int(amount))
    try:
        result = DBManager.db.session.execute(query)
        quizResult = []
        for item in result :
            quizResult.append(item)
    except:
        print traceback.print_exc()

    #보기용으로 amount*4
    query = "select idWord, chinese, pronunciation, meaning from word order by rand() limit %d;"%(int(amount)*4)
    try:
        result = DBManager.db.session.execute(query)
        exampleResult = []
        for item in result :
            exampleResult.append(item)
    except:
        print traceback.print_exc()

    returnQuizData = []
    for quiz in quizResult :
        quizExampleList = []
        quizExample = {"idWord": quiz[0], "chinese": quiz[1],"pronunciation":quiz[2], "meaning":quiz[3]}
        quizExampleList.append(quizExample)
        while len(quizExampleList) < 4 :
            example = exampleResult.pop()
            if quiz[0] != example[0]:
                quizExample = {"idWord": example[0], "chinese": example[1],"pronunciation":example[2], "meaning":example[3]}
                quizExampleList.append(quizExample)

        #end while

        quizItem = {"idWord": quiz[0], "chinese": quiz[1],"pronunciation":quiz[2],
                "meaning":quiz[3],"difficulty":quiz[4],"example":quizExampleList}
        returnQuizData.append(quizItem)
    #end for


    return jsonify(result=returnQuizData)
"""
    result_list = []
    for item in result:
        item = {"idWord": item[0], "chinese": item[1],"pronunciation":item[2],
                "pro_file_path":item[3],"meaning":item[4],"difficulty":item[5],
                "imgSrc":item[6],"description":item[7],
                "createByUserId":item[8],"designByUserId":item[9]}
        result_list.append(item)
"""




def selectCheckWord(dbCheckList):
    result_list = []
    for i in range(len(dbCheckList)):
        query = "select * from word where chinese like'%"+(dbCheckList[i]).decode('utf-8')+"%'"
        try:
            result = DBManager.db.session.execute(query)
        except:
            print traceback.print_exc()
        for item in result:
            item = {"idWord": item[0], "chinese": item[1],"pronunciation":item[2],
                    "pro_file_path":item[3],"meaning":item[4],"difficulty":item[5]}
            result_list.append(item)
    return jsonify(result=result_list)




