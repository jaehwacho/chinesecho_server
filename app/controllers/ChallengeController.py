#!/usr/bin/python
# -*- coding: utf-8 -*
# -

import traceback
from app.database import DBManager


#그림이 등록되어있고 랭크스코어가 높은순으로 10개[랜덤, 서로다른단어]
def selectHardChallenge():
    from app.commons.resultDataPaser import resultImageCard

    query = """
            select word.*, target.imgSrc, target.description,
                target.createByUserId, target.designByUserId from (
                    select a.*, b.rankScore from
                        stylized_image_contents as a,
                        contents_rank as b where
                            a.idStylizedImageContents = b.idStylizedImageContents and b.rankScore >= 0.5
                            order by rand() desc)
                as target, word where target.idWord = word.idWord
            group by target.idWord order by rand() limit 10
            """
    try:
        result = DBManager.db.session.execute(query)
    except:
        print traceback.print_exc()

    return resultImageCard(result)




#그림이 등록되어있고 랭크스코어가 낮은 10개[랜덤, 서로다른단어, 베스트 제외] ## 베스트 제외하는 쿼리로 수정해야함
def selectEasyChallenge():
    from app.commons.resultDataPaser import resultImageCard
    query = """
            select word.*, target.imgSrc, target.description,
                target.createByUserId, target.designByUserId from (
                    select a.*, b.rankScore from
                        stylized_image_contents as a,
                        contents_rank as b where
                            a.idStylizedImageContents = b.idStylizedImageContents and b.rankScore < 0.5
                            order by rand() desc)
                as target, word where target.idWord = word.idWord
            group by target.idWord order by rand() limit 10
            """

    try:
        result = DBManager.db.session.execute(query)
    except:
        print traceback.print_exc()

    return resultImageCard(result)

#아무런 아이디어가 등록되지 않은 글자들
def selectNoneChallenge():
    from app.commons.resultDataPaser import resultNoneImageCard
    query = """
            select * from word where not exists
                (select 'x' from stylized_image_contents
                where word.idWord=stylized_image_contents.idWord)
            order by rand() limit 10
            """

    try:
        result = DBManager.db.session.execute(query)
    except:
        print traceback.print_exc()

    return resultNoneImageCard(result)