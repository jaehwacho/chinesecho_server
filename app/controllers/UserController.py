import traceback
from flask import jsonify
from app import DBManager, ServerConfig


def selectMyIdea(userId):
    result_list = []
    query =   """
                select a.email, a.firstName, a.lastName, a.userName,
                b.*, c.chinese, c.pronunciation, c.pro_file_path, c.meaning
                from user as a, stylized_image_contents as b, word as c
                where a.email='%s' and a.email=b.createByUserID and b.idWord = c.idWord limit 5;
             """ % (userId)
    try:
            result = DBManager.db.session.execute(query)
            if(result):
                for item in result:
                        item = {"email": item[0], "firstName": item[1],"lastName": item[2],"userName": item[3],"idStylizedImageContents": item[4],"idWord": item[5]
                            ,"imgSrc": ServerConfig.SERVER_IP+ServerConfig.IMG_SRC+item[6],"description": item[7],"createByUserId": item[8],"designByUserId": item[9],"chinese":item[11],
                                "pronunciation":item[12],"pro_file_path":item[13],"meaning":item[14]}
                        result_list.append(item)
            else :
                result_list.append(None)
    except:
            print traceback.print_exc()

    return jsonify(result=result_list)
