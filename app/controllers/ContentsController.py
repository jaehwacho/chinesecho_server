#!/usr/bin/python
# -*- coding: utf-8 -*
# -
import traceback

from flask import jsonify

from app.database import DBManager
from app.serverConfig import ServerConfig

def selectLeaningWord(userId,amount):
    print amount
    from app.commons.resultDataPaser import resultLearningImageCard
    #난이도 2이하의 모든 카드들중 10개
    query = """
                select wordData.*, (select count(*) from stylized_image_contents where idWord = wordData.idWord) as imgCount from
                (select word.*, target.imgSrc, target.description,
                target.createByUserId, target.designByUserId from (
                    select a.*, b.rankScore from
                        stylized_image_contents as a,
                        contents_rank as b where
                            a.idStylizedImageContents = b.idStylizedImageContents
                            order by a.idWord, b.rankScore desc)
                as target, word where target.idWord = word.idWord and word.difficulty = 1
            group by target.idWord) as wordData order by rand() limit %d;
            """%(int(amount))



    try:
        result = DBManager.db.session.execute(query)
    except:
        print traceback.print_exc()
    return resultLearningImageCard(result)#jsonify(result=result_list)
"""
    result_list = []
    for item in result:
        item = {"idWord": item[0], "chinese": item[1],"pronunciation":item[2],
                "pro_file_path":item[3],"meaning":item[4],"difficulty":item[5],
                "imgSrc":item[6],"description":item[7],
                "createByUserId":item[8],"designByUserId":item[9]}
        result_list.append(item)
"""




def selectCheckWord(dbCheckList):
    result_list = []
    for i in range(len(dbCheckList)):
        query = "select * from word where chinese like'%"+(dbCheckList[i]).decode('utf-8')+"%'"
        try:
            result = DBManager.db.session.execute(query)
        except:
            print traceback.print_exc()
        for item in result:
            item = {"idWord": item[0], "chinese": item[1],"pronunciation":item[2],
                    "pro_file_path":item[3],"meaning":item[4],"difficulty":item[5]}
            result_list.append(item)
    return jsonify(result=result_list)




def selectAnotherCard(wordId,userId):
    result_list = []
    query =     """
                select anotherCard.*, voteUser.idVote from
                    (select b.*, c.rankScore from word , stylized_image_contents as b, contents_rank as c where word.idWord = %s and word.idWord = b.idWord
                            and b.idStylizedImageContents = c.idStylizedImageContents order by c.rankScore desc
                ) as anotherCard
                LEFT OUTER JOIN
                    (select * from vote where vote.idUser = %s) as voteUser
                on anotherCard.idStylizedImageContents = voteUser.idStylizedImageContents
                """ % (wordId,userId)
    try:
            result = DBManager.db.session.execute(query)
    except:
            print traceback.print_exc()
    for item in result:
            item = {"idStylizedImageContents": item[0], "idWord": item[1],"imgSrc":ServerConfig.SERVER_IP+ServerConfig.IMG_SRC+item[2],
                    "description":item[3],"createByUserId":item[4],"designByUserId":item[5], "idVote":item[6]}
            result_list.append(item)

    return jsonify(result=result_list)

