#!/usr/bin/python
# -*- coding: utf-8 -*-
import hashlib

import json
from datetime import timedelta

from flask import Flask, request, make_response, jsonify
from . import errors
from flask.ext.cors import CORS, cross_origin
from flask.ext.script import Manager
import flask
from itsdangerous import URLSafeTimedSerializer

from app.serverConfig import ServerConfig

app = Flask(__name__, static_url_path='/assets', static_folder='assets')
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['DEBUG'] = True
app.secret_key = "c!h@i#n$e%s^e&c*h(o)"
app.config["REMEMBER_COOKIE_DURATION"] = timedelta(days=14)

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
db_url = ServerConfig.DB_URL + ServerConfig.DB_USER_INFO  + ServerConfig.DB_HOSTNAME
app.config['SQLALCHEMY_DATABASE_URI'] = db_url


manager = Manager(app)

from flask.ext.login import LoginManager, login_user, logout_user, login_required, current_user

login_serializer = URLSafeTimedSerializer(app.secret_key)
login_manager = LoginManager()
login_manager.init_app(app)


def hash_pass(password):
    salted_password = password + app.secret_key
    return hashlib.sha224(salted_password).hexdigest()

from app.database import DBManager
DBManager.init(app)

# Flask-Restless를 이용한 API 생성
from flask.ext.restless import APIManager
manager = APIManager(app, flask_sqlalchemy_db=DBManager.db)

from model import *
#DB초기화
#DBManager.init_db()
#user RESTful API
manager.create_api(User
                    , methods=['GET','PUT','PATCH','POST','DELETE']
                    , allow_patch_many=True
                    , url_prefix='/api/v1.0'
                    , collection_name='user'
                    , results_per_page=20)

#word RESTful API
manager.create_api(Word
                    , methods=['GET','PUT','PATCH','POST','DELETE']
                    , allow_patch_many=True
                    , url_prefix='/api/v1.0'
                    , collection_name='word'
                    , results_per_page=20)

#StylizedImageContent RESTful API
manager.create_api(StylizedImageContent
                    , methods=['GET','PUT','PATCH','POST','DELETE']
                    , allow_patch_many=True
                    , url_prefix='/api/v1.0'
                    , collection_name='stylized_image_contents'
                    , results_per_page=20)

#ContentsRank RESTful API
manager.create_api(ContentsRank
                    , methods=['GET','PUT','PATCH','POST','DELETE']
                    , allow_patch_many=True
                    , url_prefix='/api/v1.0'
                    , collection_name='contents_rank'
                    , results_per_page=20)


@login_manager.user_loader
def load_user(email):
    return User.query.filter_by(email=email).first()


@login_manager.request_loader
def load_user_from_request(request):
    # first, try to login using the api_key url arg
    api_key = request.args.get('api_key')
    print api_key
    if api_key:
        user = User.query.filter_by(api_key=str(api_key)).first()
        if user:
            return user
    api_key = json.loads(request.data)['api_key']
    print api_key
    if api_key:
        user = User.query.filter_by(api_key=str(api_key)).first()
        if user:
            return user
    # finally, return None if both methods did not login the user
    return None


@login_manager.token_loader
def load_token(token):
    max_age = app.config["REMEMBER_COOKIE_DURATION"].total_seconds()
    data = login_serializer.loads(token, max_age=max_age)
    user = User.get(data[0])
    if user and data[1] == user.password:
        return user
    return None

#로그아웃
@app.route("/api/v1.0/logout/", methods=['GET'])
@login_required
def doLogout():
    user_id = (current_user.get_id() or "No User Logged In")
    print user_id
    user = User.query.filter_by(email=user_id).first()
    user.api_key = None
    DBManager.db.session.commit()
    logout_user()

    result= "[{'status' : 'logout'}]"
    response = make_response(result)
    return response

#로그인
@app.route("/api/v1.0/login/", methods=["POST"])
def doLogin():
        user = User.query.filter_by(email=json.loads(request.data)['email']).first()
        if user and hash_pass(json.loads(request.data)['password']) == user.password:
            login_user(user, remember=True)
            user.api_key = current_user.get_auth_token()
            DBManager.db.session.commit()
            print current_user.email
            result = '{"status" : "'+user.api_key+'"}'
            response = make_response(result)
            return response
        else :
            result = '{"status" : false}'
            response = make_response(result)
            return response

#회원가입
@app.route("/api/v1.0/signup/", methods=["POST"])
def doSignUp():
        user = User.query.filter_by(email=json.loads(request.data)['email']).first()
        print user
        if user :
            result = '{"status" : false}'
            response = make_response(result)
            return response
        else :
            user = User(json.loads(request.data)['email'],hash_pass(json.loads(request.data)['password']))
            DBManager.db.session.add(user)
            DBManager.db.session.commit()
            login_user(user, remember=True)
            user.api_key = current_user.get_auth_token()
            DBManager.db.session.commit()
            print current_user.email
            result = '{"status" : "'+user.api_key+'"}'
            response = make_response(result)
            return response



#단어학습 API
@app.route("/api/v1.0/wordlearning/<id>/", methods=['GET'])
@login_required
#@cross_origin() # allow all origins all methods.
def getWordData(id):
    from controllers.ContentsController import selectLeaningWord
    amount = request.args.get('amount')
    result = selectLeaningWord(id,amount);
    response = make_response(result)
    return response

@app.route("/api/v1.0/anothercard/<wordid>", methods=['GET'])
@login_required
#@cross_origin() # allow all origins all methods.
def getAnotherCardData(wordid):
    from controllers.ContentsController import selectAnotherCard
    result = selectAnotherCard(wordid,current_user.idUser);
    response = make_response(result)
    return response

#단어학습 로그입력 API
@app.route("/api/v1.0/uploadLearningLog/", methods=['POST'])
@cross_origin() # allow all origins all methods.
@login_required
def uploadLearningLog() :
    user = current_user.idUser
    data = json.loads(json.loads(request.data)['resultData'])
    for item in data:
        examLog = LearningLog(None,item['idWord'],item['stayTime'],item['activateSound'],item['time'],item['activateGood'],item['activateBad'],user)
        DBManager.db.session.add(examLog)

    DBManager.db.session.commit()
    result = '{"status":"success"}'
    response = make_response(result)
    return response


#시험보기 API
@app.route("/api/v1.0/wordexam", methods=['GET'])
@login_required
#@cross_origin() # allow all origins all methods.
def getExamData():
    from controllers.ExamController import selectExamWord
    amount = request.args.get('amount')
    result = selectExamWord(current_user.get_id(),amount);
    response = make_response(result)
    return response

#시험결과 및 로그입력 API
@app.route("/api/v1.0/uploadExamLog/", methods=['POST'])
@cross_origin() # allow all origins all methods.
@login_required
def uploadExamLog() :
    user = current_user.idUser
    data = json.loads(json.loads(request.data)['resultData'])
    for item in data:
        examLog = ExamLog(None,item['idWord'],item['stayTime'],item['time'],item['activateSure'],item['checkResult'],item['checkId'],user)
        DBManager.db.session.add(examLog)

    DBManager.db.session.commit()
    result = '{"status":"success"}'
    response = make_response(result)
    return response


#필기인식 API
@app.route("/api/v1.0/handwritingcheck/", methods=['POST'])
@cross_origin() # allow all origins all methods.
@login_required
def handwritingcheck():
    from commons.zinniaModule import checkWord
    from controllers.ContentsController import selectCheckWord
    result = checkWord(json.loads(request.data)['sexpData'])
    dbCheck = []
    wordList = "爱矮安静拔爸把吧八爸爸白百半帮助抱饱包报纸北北京杯子本鼻笔比便宜别不不客气不错菜草草莓茶查差尝长唱唱歌吵车成吃迟到臭丑出船穿川吹春春天出租汽车次从聪明错达大打打电话打篮球大夫大家但是到倒刀大学的得等灯低电点电脑电视电影弟弟第一动懂冬天东西豆都读短对对不起多多少饿二儿子法发反方方便房间饭馆发现飞非常飞机分风分钟父服务员复习改干净高告诉高兴各个哥哥给歌手红公共汽车公斤公司工作狗家关广光贵归国过还孩子行韩国汉语号好好吃和喝黑很红后后面画花坏画家还黄欢迎回会回答伙火火车站急几加见件见面叫教教室机场鸡蛋借接姐姐介绍近进今天旧就久九觉得卡咖啡开开始看看见考试课渴可爱可能可怕可以口苦哭快块快乐辣拉来老老师了乐累泪累冷离力里零两聊天儿零六路绿旅行旅游吗马妈妈妈卖买妈妈慢忙满足毛猫没每没关系美国美丽妹妹门米饭名明天名字目木母哪那奶奶南难男人呢能你你好年你们您牛牛奶女女儿暖和女人爬怕胖旁边跑跑步朋友便宜票漂亮苹果骑七起床前钱千前面晴请轻秋天妻子去去年让热人认识日日本入伞三色山上上班商店上午上学少扫谁什么声生病生日身体十是石时候事情时间手手表手机树书帅书包水水果睡觉说说话四死送岁些所以他她它太他们题踢天天气跳舞听踢足球同同学头头发土脱外完玩万晚晚上王喂为什么问问题我我们五洗西喜欢下下雨先先生向想香香蕉现在笑小小姐小时夏天下午下学写谢谢西瓜信心新姓星期水果星期天星期一兄休息希望学雪学生学习学校眼眼睛羊阳羊肉眼睛眼镜颜色要药爷夜也爷爷一衣衣服已经意思音阴因为一起医生医院椅子用右有友右边游泳鱼雨元远月云运动预习在再再见早早上怎么怎么样站张丈夫找着这真正正在止知知道重众中中国中午舟住祝准备桌子字自行车走足最坐做左座左边昨天"
    for i in range(len(result)):
        if wordList.find(result[i]) != -1:
            print result[i]
            dbCheck.append(result[i])

    response_result = selectCheckWord(dbCheck)
    response = make_response(response_result)
    return response

#ChallengeAPI
@app.route("/api/v1.0/challenge/<flag>/", methods=['GET'])
@cross_origin() # allow all origins all methods.
@login_required
def getChallengeData(flag):
    #checkData = request.args.get('flag')
    #print checkData=="hard"
    if flag=="hard":
        from controllers.ChallengeController import selectHardChallenge
        result = selectHardChallenge()
    elif flag=="easy":
        from controllers.ChallengeController import selectEasyChallenge
        result = selectEasyChallenge()
    elif flag=="none":
        from controllers.ChallengeController import selectNoneChallenge
        result = selectNoneChallenge()
    else :
        result = '{"status" : false}'
    response = make_response(result)
    return response

#Challange 등록 및 파일업로드 API
@app.route("/api/v1.0/uploadchallenge/", methods=['POST'])
@cross_origin() # allow all origins all methods.
@login_required
def uploadPhoto() :
    import base64, os, uuid
    user = User.query.filter_by(email=str(current_user.email)).first()
    idWord = json.loads(request.data)['idWord']
    imgDesc = json.loads(request.data)['descData']
    fileName = str(uuid.uuid4())+".png"
    filePath = app.root_path+"/assets/userIdea/"+fileName
    base64String = json.loads(request.data)['image']
    base64String = base64String.split(',')[1]
    imgData = base64.decodestring(base64String)

    if os.path.isfile(filePath) :
        result = '{"status":"filenameError"}'
    else :
        try:
            f = open(filePath,"w")
            f.write(imgData)
            f.close()
            result = '{"status":"success"}'
            imageContents = StylizedImageContent(None,idWord,fileName,imgDesc,user.email,None)
            DBManager.db.session.add(imageContents)
            DBManager.db.session.commit()

        except IOError, e:
            result = e

    response = make_response(result)
    return response


#내 아이디어 가져오기
@app.route("/api/v1.0/user/myidea", methods=['GET'])
@cross_origin() # allow all origins all methods.
@login_required
def selectMyIdeas() :
    from controllers.UserController import selectMyIdea
    result = selectMyIdea(current_user.get_id());
    response = make_response(result)
    return response




if __name__ == '__main__':
    app.run()
