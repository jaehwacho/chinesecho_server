#!/usr/bin/python
# -*- coding: utf-8 -*-

from sqlalchemy import Column,Integer, String
from sqlalchemy.orm import relationship
from app.model import db

class Word(db.Model):
    __tablename__ = 'word'

    idWord = db.Column(db.Integer, primary_key=True, autoincrement=True)
    chinese = db.Column(db.String(32))
    pronunciation = db.Column(db.String(32))
    pro_file_path = db.Column(db.String(128), default='')
    meaning = db.Column(db.String(32))
    difficulty = db.Column(db.Integer)

    #stylized_image_contents = relationship("StylizedImageContent", backref="stylized_image_contents")
    stylized_image_contents = relationship("StylizedImageContent", backref="word")


    def __init__(self
                 , idWord
                 , chinese
                 , pronunciation
                 , pro_file_path
                 , meaning
                 , difficulty):

        self.idWord = idWord
        self.chinese = chinese
        self.pronunciation = pronunciation
        self.pro_file_path = pro_file_path
        self.meaning = meaning
        self.difficulty = difficulty

    def __repr__(self):
        return '<word %r>' % (self.idWord)