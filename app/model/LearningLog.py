#!/usr/bin/python
# -*- coding: utf-8 -*-
from sqlalchemy import Column,Integer, ForeignKey, Table
from app.model import db


class LearningLog(db.Model):
    __tablename__ = 'learning_log'
    idLearningLog = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idWord = db.Column(db.Integer, ForeignKey('word.idWord'))
    stayTime = db.Column(db.Integer)
    activateSound = enabled = db.Column(db.Boolean, default=False)
    time = db.Column(db.DateTime)
    activateGood = enabled = db.Column(db.Boolean, default=False)
    activateBad = enabled = db.Column(db.Boolean, default=False)
    idUser = db.Column(db.Integer, ForeignKey('user.idUser'))

    def __init__(self
                 , idLearningLog
                 , idWord
                 , stayTime
                 , activateSound
                 , time
                 , activateGood
                 , activateBad
                 , idUser):

        self.idLearningLog = idLearningLog
        self.idWord = idWord
        self.stayTime = stayTime
        self.activateSound = activateSound
        self.time = time
        self.activateGood = activateGood
        self.activateBad = activateBad
        self.idUser = idUser
