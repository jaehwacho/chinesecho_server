#!/usr/bin/python
# -*- coding: utf-8 -*-
from sphinx.websupport.storage.sqlalchemy_db import Base
from datetime import *
from sqlalchemy import Column,Integer, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from app.model import db


class StylizedImageContent(db.Model):
    __tablename__ = 'stylized_image_contents'

    idStylizedImageContents = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idWord = db.Column(db.Integer, ForeignKey('word.idWord'))
    imgSrc = db.Column(db.String(128), unique=True)
    description = db.Column(db.String(48))
    #createByUserId = db.Column(db.Integer, ForeignKey('user.idUser'))
    createByUserId = db.Column(db.String(24))
    designByUserId = db.Column(db.String(24))
    createDate = db.Column(db.DateTime, default=datetime.now)
        #db.Column(Integer, ForeignKey('user.idUser'))

    vote = relationship("Vote", backref="vote.idStylizedImageContents")
    #contents_rank = relationship("ContentsRank", backref="contents_rank.idStylizedImageContents")
    contents_rank = relationship("ContentsRank",uselist=False, backref="stylized_image_contents")


    def __init__(self
                 , idStylizedImageContents
                 , idWord
                 , imgSrc
                 , description
                 , createByUserId
                 , designByUserId):

        self.idStylizedImageContents = idStylizedImageContents
        self.idWord = idWord
        self.imgSrc = imgSrc
        self.description = description
        self.createByUserId = createByUserId
        self.designByUserId = designByUserId

class ContentsRank(db.Model):
    __tablename__ = 'contents_rank'
    idContentsRank = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idStylizedImageContents = db.Column(db.Integer, ForeignKey('stylized_image_contents.idStylizedImageContents'))
    #idStylizedImageContents = db.Column(db.Integer)
    rankScore = db.Column(db.Float)

    def __init__(self
                 , idContentsRank
                 , idStylizedImageContents
                 , rankScore):
        self.idContentsRank = idContentsRank
        self.idStylizedImageContents = idStylizedImageContents
        self.rankScore = rankScore