#!/usr/bin/python
# -*- coding: utf-8 -*-
from datetime import *
from sqlalchemy import ForeignKey
from app.model import db

class Vote(db.Model):
    __tablename__ = 'vote'

    idVote = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idUser = db.Column(db.Integer, ForeignKey('user.idUser'))
    idStylizedImageContents = db.Column(db.Integer, ForeignKey('stylized_image_contents.idStylizedImageContents'))
    voteDate = db.Column(db.DateTime, default=datetime.now)


    def __init__(self
                 , idVote
                 , idUser
                 , idStylizedImageContents
                 , voteDate):

        self.idVote = idVote
        self.idUser = idUser
        self.idStylizedImageContents = idStylizedImageContents
        self.voteDate = voteDate
