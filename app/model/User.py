#!/usr/bin/python
# -*- coding: utf-8 -*-
from datetime import *
from sqlalchemy.orm import relationship
from app import app, login_serializer
from app.model import db

class User(db.Model):
    __tablename__ = 'user'

    idUser = db.Column(db.Integer, primary_key=True, autoincrement=True)
    #idGroup = db.Column(db.String(32))
    email = db.Column(db.String(30), unique=True)
    firstName = db.Column(db.String(16))
    lastName = db.Column(db.String(16))
    userName = db.Column(db.String(24))
    password = db.Column(db.String(64))
    createdAt = db.Column(db.DateTime, default=datetime.now)
    enabled = db.Column(db.Boolean, default=True)

    isTeacher = db.Column(db.Boolean, default=False)
    isDesigner = db.Column(db.Boolean, default=False)

    vote = relationship("Vote", backref="vote.idUser")
    api_key = db.Column(db.String(128))
    #create_image_contents = relationship("StylizedImageContent", backref="stylized_image_contents.createByUserId")
    #design_image_contents = relationship("StylizedImageContent", backref="stylized_image_contents.designByUserId")
    #device_log = db.relationship('DeviceLog', backref='device', lazy='dynamic')

    def __init__(self
                 , email
                 , password
                 ):

        self.email = email
        self.password = password

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.email

    # Required for administrative interface
    def __unicode__(self):
        return self.userName

    def get_auth_token(self):
        data = [str(self.email), self.password]
        return login_serializer.dumps(data)