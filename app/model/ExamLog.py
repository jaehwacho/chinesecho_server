#!/usr/bin/python
# -*- coding: utf-8 -*-
from sqlalchemy import Column,Integer, ForeignKey, Table
from app.model import db


class ExamLog(db.Model):
    __tablename__ = 'exam_log'
    idExamLog = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idWord = db.Column(db.Integer, ForeignKey('word.idWord'))
    stayTime = db.Column(db.Integer)
    time = db.Column(db.DateTime)
    activateSure = db.Column(db.Boolean, default=False)
    checkResult = db.Column(db.Boolean, default=False)
    checkWordId = db.Column(db.Integer)
    idUser = db.Column(db.Integer, ForeignKey('user.idUser'))

    def __init__(self
                 , idExamLog
                 , idWord
                 , stayTime
                 , time
                 , activateSure
                 , checkResult
                 , checkWordId
                 , idUser):

        self.idExamLog = idExamLog
        self.idWord = idWord
        self.stayTime = stayTime
        self.time = time
        self.activateSure = activateSure
        self.checkResult = checkResult
        self.checkWordId = checkWordId
        self.idUser = idUser
