#!/usr/bin/python
# -*- coding: utf-8 -*-

class ServerConfig(object):
    #: 데이터베이스 연결 URL
    DB_URL= 'mysql://'
    #: id/password
    DB_USER_INFO = 'root:tous3186'
    #소마 테스트용
    #SERVER_IP = 'http://172.16.100.155:5000'
    #외부용
    SERVER_IP = 'http://191.238.81.153:5000'
    #: 데이터베이스 파일 경로, 외부주소
    #DB_HOSTNAME= '@191.238.81.167/dochinese'
    #내부 주소
    DB_HOSTNAME= '@100.79.172.16/dochinese'

    #: 사진 업로드 시 사진이 임시로 저장되는 임시 폴더
    TMP_FOLDER = 'resource/tmp/'
    #: 업로드 완료된 사진 파일이 저장되는 폴더
    UPLOAD_FOLDER = 'resource/upload/'
    #: 업로드되는 사진의 최대 크키(3메가)
    MAX_CONTENT_LENGTH = 10 * 1024 * 1024
    #: 세션 타임아웃은 초(second) 단위(60분)
    PERMANENT_SESSION_LIFETIME = 60 * 60
    #: 쿠기에 저장되는 세션 쿠키
    SESSION_COOKIE_NAME = 'chinesecho_session'
    #: 로그 레벨 설정
    LOG_LEVEL = 'debug'
    #: 디폴트 로그 파일 경로
    LOG_FILE_PATH = 'resource/log/chinesecho.log'
    #: 디폴트 SQLAlchemy trace log 설정
    DB_LOG_FLAG = 'True'
    #: 디폴트 이미지 폴더 위치
    IMG_SRC = '/assets/userIdea/'

